USE SuperheroDb

INSERT INTO Power(Name,Description)
VALUES('Super strength', 'strength to lift all grocery bags at once!');

INSERT INTO Power(Name,Description)
VALUES('Flight', 'The power to fly or levitate');

INSERT INTO Power(Name,Description)
VALUES('Loves peace?', 'Really loves peace');


INSERT INTO HeroPower(HeroID, PowerID)
VALUES(3,1);

INSERT INTO HeroPower(HeroID, PowerID)
VALUES(3,2);

INSERT INTO HeroPower(HeroID, PowerID)
VALUES(4,1);

INSERT INTO HeroPower(HeroID, PowerID)
VALUES(5,6);