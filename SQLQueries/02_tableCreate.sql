USE SuperheroDb

CREATE TABLE Superhero(
	ID int PRIMARY KEY IDENTITY(1,1),
	Name nvarchar(50) null,
	Alias nvarchar(50) null,
	Origin nvarchar(50) null
)


CREATE TABLE Assistant(
	ID int PRIMARY KEY IDENTITY(1,1),
	Name nvarchar(50)
)


CREATE TABLE Power(
	ID int PRIMARY KEY IDENTITY(1,1),
	Name nvarchar(50),
	Description nvarchar(50)
)