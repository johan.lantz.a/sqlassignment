﻿using Microsoft.Data.SqlClient;
using SQLAssigment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLAssigment.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        public bool CreateNewCustomer(Customer customer)
        {
            throw new NotImplementedException();
        }

        public List<Customer> GetAllCustomers()
        {
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    connection.Open();
                    //command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        //reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                //Handle result
                                Customer temp = new Customer();
                                

                               

                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3)) temp.Country = reader.GetString(3);
                                if (!reader.IsDBNull(4)) temp.PostalCode = reader.GetString(4);
                                if (!reader.IsDBNull(5)) temp.Phone = reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                customerList.Add(temp);
                            }
                        }
                    }
                    

                 
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Something went wrong");
                Console.WriteLine(ex.Message);
            }
        


         

            return customerList;
        }

        public List<Customer> GetCustomerByCountry()
        {
            throw new NotImplementedException();
        }

        public List<Customer> GetCustomerBySpending()
        {
            throw new NotImplementedException();
        }

        public List<Customer> GetPageOfCustomer(int limit, int offset)
        {
            throw new NotImplementedException();
        }

        public Customer GetSpecificCustomer(int id)
        {
            throw new NotImplementedException();
        }

        public Customer GetSpecificCustomer(string name)
        {
            throw new NotImplementedException();
        }

        public void UpdateCustomer()
        {
            throw new NotImplementedException();
        }
    }
}
