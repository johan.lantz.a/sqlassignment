﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLAssigment.Repositories
{
    public class ConnectionHelper
    {
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

            builder.DataSource = @"(localdb)\MSSQLLocalDB";
            builder.InitialCatalog = "Chinook";
            builder.IntegratedSecurity = true;
            builder.Encrypt = false;
            builder.TrustServerCertificate = true;

            return builder.ConnectionString;
        }
    }
}
