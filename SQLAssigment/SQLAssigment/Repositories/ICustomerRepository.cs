﻿using SQLAssigment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLAssigment.Repositories
{
    public interface ICustomerRepository
    {
        public Customer GetSpecificCustomer(int id);

        public Customer GetSpecificCustomer(string name);

        public bool CreateNewCustomer(Customer customer);

        public List<Customer> GetAllCustomers();

        public List<Customer> GetPageOfCustomer(int limit, int offset);

        public void UpdateCustomer();

        public List<Customer> GetCustomerByCountry();

        public List<Customer> GetCustomerBySpending();
    }
}
