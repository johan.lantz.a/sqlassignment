﻿
using Microsoft.Data.SqlClient;
using SQLAssigment.Models;
using SQLAssigment.Repositories;



ICustomerRepository repository = new CustomerRepository();

testSelectAll(repository);


static void testSelectAll(ICustomerRepository repository)
{
    PrintCustomers(repository.GetAllCustomers());
}

static void testSelect(ICustomerRepository repository)
{
    PrintCustomer(repository.GetSpecificCustomer(""));
}

static void testInsert(ICustomerRepository repository)
{
    Customer test = new Customer()
    {
        FirstName = "ThisIsMyFirstName",
        LastName = "ThisIsMySurname",
        Email = "testytester@tester.com"
    };
    if (repository.CreateNewCustomer(test))
    {
        Console.WriteLine("New Customer added");
    }
    else
    {
        Console.WriteLine("Could not create new customer");
    }
}


static void PrintCustomers(IEnumerable<Customer> customers)
{
    foreach (Customer customer in customers)
    {
        PrintCustomer(customer);
    }
}

static void PrintCustomer(Customer customer)
{
    Console.WriteLine($"----{customer.CustomerId} {customer.FirstName} {customer.LastName} Country: {customer.Country} Postal code: {customer.PostalCode} Phone: {customer.Phone} Email: {customer.Email} ");
}